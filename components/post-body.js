import BlockContent from "@sanity/block-content-to-react";
import DocumentDownloadIcon from "@heroicons/react/outline/DocumentDownloadIcon";
import ArrowRightIcon from "@heroicons/react/outline/ArrowRightIcon";
import markdownStyles from "./markdown-styles.module.css";

export default function PostBody({ content }) {
  return (
    <div className="max-w-2xl mx-auto">
      <BlockContent
        blocks={content.filter((block) => block._type !== "sourceLink")}
        className={`prose prose-sm sm:prose-base md:prose-lg xl:prose-xl 2xl:prose-2xl`}
      />
      <BlockContent
        blocks={content.filter((block) => block._type === "sourceLink")}
        className="mt-4 p-2 flex flex-wrap flex-col sm:flex-row divide-y sm:divide-y-0 sm:divide-x overflow-hidden border-2 rounded-lg"
        serializers={{
          types: {
            sourceLink: (props) => (
              <a
                target="_blank"
                href={props.node.url}
                className={`p-1 flex-1 hover:bg-green-50 rounded-lg ${
                  !props.node.url ? "text-gray-400" : "text-green-700"
                }`}
              >
                <div className="p-3 flex-1 flex justify-center items-center space-x-3">
                  <div className="text-2xl text-center">{props.node.label}</div>
                  {props.node.url &&
                    (props.node.url?.includes("firebase") ? (
                      <DocumentDownloadIcon className="h-5 w-5" />
                    ) : (
                      <ArrowRightIcon className="h-5 w-5" />
                    ))}
                </div>
              </a>
            ),
          },
        }}
      />
    </div>
  );
}
