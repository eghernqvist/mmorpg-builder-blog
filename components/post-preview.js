import Avatar from "../components/avatar";
import Date from "../components/date";
import CoverImage from "./cover-image";
import Link from "next/link";

export default function PostPreview({
  title,
  coverImage,
  date,
  excerpt,
  slug,
  upcoming,
}) {
  return (
    <div>
      {coverImage && (
        <div className="mb-5">
          <CoverImage slug={slug} title={title} image={coverImage} />
        </div>
      )}
      <h3 className="text-3xl mb-3 leading-snug">
        {upcoming ? (
          title
        ) : (
          <Link href={`/posts/${slug}`}>
            <a className="hover:underline">{title}</a>
          </Link>
        )}
      </h3>
      {date && (
        <div className="text-lg mb-4">
          <Date dateString={date} />
        </div>
      )}
      <p className="text-lg leading-relaxed mb-4">{excerpt}</p>
    </div>
  );
}
