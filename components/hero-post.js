import Avatar from "../components/avatar";
import Date from "../components/date";
import CoverImage from "../components/cover-image";
import Link from "next/link";

export default function HeroPost({
  title,
  coverImage,
  date,
  slug,
  upcoming,
}) {
  console.log('upcoming', upcoming);
  return (
    <section>
      <h2 className="mb-8 text-6xl md:text-7xl font-bold tracking-tighter leading-tight">
        Latest post
      </h2>
      {coverImage && (
        <div className="mb-8 md:mb-16">
          <CoverImage slug={slug} title={title} image={coverImage} />
        </div>
      )}
      <div className="md:grid md:col-gap-16 lg:col-gap-8 mb-20 md:mb-28">
        <div>
          <h3 className="mb-4 text-4xl lg:text-6xl leading-tight">
            {upcoming ? (
              <a className="hover:underline">{title}</a>
            ) : (
              <Link href={`/posts/${slug}`}>
                <a className="hover:underline">{title}</a>
              </Link>
            )}
          </h3>
          <div className="mb-4 md:mb-0 text-lg">
            <Date dateString={date} />
          </div>
        </div>
      </div>
    </section>
  );
}
