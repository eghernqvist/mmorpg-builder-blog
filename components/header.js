import Link from 'next/link'
import Avatar from './avatar'

export default function Header({ name }) {
  return (
    <h2 className="text-2xl md:text-4xl font-bold tracking-tight md:tracking-tighter leading-tight mb-12 mt-8">
      <Link href="/">
        <a className="hover:underline">{name}</a>
      </Link>
    </h2>
  )
}
