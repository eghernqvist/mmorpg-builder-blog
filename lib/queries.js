import { CMS_PROJECT_ID } from "./constants";

const projectFields = `...`;

export const projectQuery = `
  *[_type == "project" && projectId.current == "${CMS_PROJECT_ID}"][0] {
    ${projectFields}
  }
`;

const postFields = `
  _id,
  name,
  title,
  date,
  excerpt,
  coverImage,
  upcoming,
  "slug": slug.current,
  "author": author->{"name": coalesce(name, "Emil"), picture},
`;

export const indexQuery = `*[_type == "project" && projectId.current == "${CMS_PROJECT_ID}"][0] {
  "posts": posts[]->{${postFields}} | order(date desc) | [upcoming != true]
} | ([...posts])`;

export const upcomingQuery = `*[_type == "project" && projectId.current == "${CMS_PROJECT_ID}"][0] {
  "posts": posts[]->{${postFields}} | order(slug asc) | [upcoming == true]
} | ([...posts])`;

export const postQuery = `
{
  "post": *[_type == "post" && slug.current == $slug][0] { content, ${postFields} },
  "upcomingPosts": *[_type == "project" && projectId.current == "${CMS_PROJECT_ID}"][0] {
    "posts": posts[]->{${postFields}} | order(slug asc) | [upcoming == true]
  } | ([...posts]),
}`;

export const postSlugsQuery = `
*[_type == "project" && projectId.current == "${CMS_PROJECT_ID}"][0] {
  "slugs": posts[defined(slug.current)].slug.current
} | ([...slugs])
`;

export const postBySlugQuery = `
*[_type == "project" && projectId.current == "${CMS_PROJECT_ID}"][0] {
  ...posts[slug.current == $slug][0] {
    ${postFields}
  }
}
`;
