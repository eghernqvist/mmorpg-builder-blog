import Head from "next/head";
import Container from "../components/container";
import MoreStories from "../components/more-stories";
import HeroPost from "../components/hero-post";
import Layout from "../components/layout";
import Header from "../components/header";
import { PROJECT_NAME } from "../lib/constants";
import { indexQuery, projectQuery, upcomingQuery } from "../lib/queries";
import { getClient, overlayDrafts } from "../lib/sanity.server";
import UpcomingPosts from "../components/upcoming-posts";

export default function Index({ project, allPosts, upcomingPosts, preview }) {
  const heroPost = allPosts[0];
  const morePosts = allPosts.slice(1);
  return (
    <>
      <Layout preview={preview}>
        <Head>
          <title>{project.name} - Home</title>
        </Head>
        <Container>
          <Header name={project.name} />
          {heroPost && (
            <HeroPost
              title={heroPost.title}
              coverImage={heroPost.coverImage}
              date={heroPost.date}
              author={heroPost.author}
              slug={heroPost.slug}
              excerpt={heroPost.excerpt}
              upcoming={heroPost.upcoming}
            />
          )}
          {morePosts.length > 0 && <MoreStories posts={morePosts} />}
          {upcomingPosts.length > 0 && <UpcomingPosts posts={upcomingPosts} />}
        </Container>
      </Layout>
    </>
  );
}

export async function getStaticProps({ preview = false }) {
  const project = await getClient(preview).fetch(projectQuery);
  const allPosts = overlayDrafts(await getClient(preview).fetch(indexQuery));
  const upcomingPosts = overlayDrafts(await getClient(preview).fetch(upcomingQuery));
  console.log('allPosts', allPosts);
  return {
    props: { project, allPosts, upcomingPosts, preview },
  };
}
